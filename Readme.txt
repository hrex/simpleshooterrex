There are 10 problems with this game. Can you fix these problems and write down your solutions?




1. There is no music when game is started

2. x does not start the game

3. Enemies are not getting killed by the players bullets

4. Player's ship can go out of the visible screen area

5. Enemy bullets will not kill the player ship

6. Make the bullets from the player's ship come out faster

7. The first enemy wave is supped to be 3 ships, but the left one is missing

8. When an enemy ship is killed, the player should get 100 points not 1

9. High score is not showing or updating

10. Make a creative addition to the game!

====================

Source code is at https://bitbucket.org/yusufpisan/simpleshooter/


You need to create a "Pull Request" in BitBucket.
See https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html if necessary.


About Forking
      When you work with another user's public Bitbucket repository, typically you have read access to the code but not write access. This is where the concept of forking comes in. Here's how it works:
      Fork the repository to copy it to your own account.
      Clone the forked repository from Bitbucket to your local system.
      Make changes to the local repository.
      Push the changes to your forked repository on Bitbucket.
      Create a pull request from the original repository you forked to add the changes you made.
      Wait for the repository owner to accept or reject your changes.


When using version control with Unity

You need to make the Meta files visible in Unity
    * Edit >Project Settings > Editor -> Version Control    Visible Meta Files 

Add the .gitignore file
    * http://kleber-swf.com/the-definitive-gitignore-for-unity-projects/ Depending on your operating system, the .gitignore file may not be visible in the Explorer, Finder, etc but you can see it at the DOS and Unix shells.

Make sure your repository is readable by 'yusufpisan'

This project already has visible meta files and a .gitignore file (as well as a visible-gitignore.txt to remind you to always add .gitignore